#ifndef __ILI9341_H__
#define __ILI9341_H__	

/**************************************************************************
		macro declaration
**************************************************************************/
#define ILI9341_PIXEL_WIDTH       320
#define ILI9341_PIXEL_HEIGHT      240
#define ILI9341_PIXEL_COUNT     ILI9341_PIXEL_WIDTH * ILI9341_PIXEL_HEIGHT


#define TFT_DC_SET      LL_GPIO_SetOutputPin(TFT_DC_GPIO_Port, TFT_DC_Pin);
#define TFT_DC_RESET    LL_GPIO_ResetOutputPin(TFT_DC_GPIO_Port, TFT_DC_Pin);

#define TFT_RST_SET     LL_GPIO_SetOutputPin(TFT_RESET_GPIO_Port, TFT_RESET_Pin);
#define TFT_RST_RESET   LL_GPIO_ResetOutputPin(TFT_RESET_GPIO_Port, TFT_RESET_Pin);

#define TFT_CS_SET      LL_GPIO_SetOutputPin(TFT_CS_GPIO_Port, TFT_CS_Pin);
#define TFT_CS_RESET    LL_GPIO_ResetOutputPin(TFT_CS_GPIO_Port, TFT_CS_Pin);

#define TFT_BLED_OFF    LL_GPIO_SetOutputPin(TFT_BLED_GPIO_Port, TFT_BLED_Pin);
#define TFT_BLED_ON     LL_GPIO_ResetOutputPin(TFT_BLED_GPIO_Port, TFT_BLED_Pin);

/**************************************************************************
		function declaration
**************************************************************************/

//--------------------------------------------------------------------------
void TFT_Write_Data8(unsigned char dat); 
//--------------------------------------------------------------------------
 void TFT_Write_Data16(unsigned short dat);	  
//--------------------------------------------------------------------------
void TFT_Write_Reg(unsigned char dat);
//--------------------------------------------------------------------------
void TFT_Set_XY(unsigned int  Xpos, unsigned int  Ypos);
//--------------------------------------------------------------------------
void TFT_Set_Region(unsigned int xStar, unsigned int yStar,unsigned int xEnd,unsigned int yEnd);
//--------------------------------------------------------------------------
void TFT_Init(void);
//--------------------------------------------------------------------------
void TFT_Draw_Point(unsigned int x0, unsigned int y0 ,unsigned short color);
//--------------------------------------------------------------------------
void TFT_Draw_Rect(unsigned int x0, unsigned int y0, unsigned int x1, unsigned int y1, unsigned short color);
//--------------------------------------------------------------------------
//更新设定区域内的数据信息
void TFT_Flush_Data(unsigned int x0, unsigned int y0, unsigned int x1, unsigned int y1, unsigned short *pData);

#endif  
	 
	 



