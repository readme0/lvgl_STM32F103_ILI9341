#include "includes.h"

//--------------------------------------------------------------------------
void SPI_Write(unsigned char dat) 
{
	unsigned int i=0;
	while(i--);//用于延时
	SPI1->DR=(unsigned short)dat;
	while(!(SPI1->SR & SPI_I2S_FLAG_TXE));
}
//--------------------------------------------------------------------------
void TFT_Write_Data8(unsigned char dat)
{
	TFT_SET_DC;
	SPI_Write(dat);
}  
//--------------------------------------------------------------------------
 void TFT_Write_Data16(unsigned int dat)
{
	TFT_SET_DC;
	SPI_Write(dat>>8);
	SPI_Write(dat);
}	  
//--------------------------------------------------------------------------
void TFT_Write_Reg(unsigned char dat)	 
{
	TFT_RESET_DC;
	SPI_Write(dat);
}

/*************************************************
函数名：TFT_Set_XY
功能：设置lcd显示起始点
入口参数：xy坐标
返回值：无
*************************************************/
void TFT_Set_XY(unsigned int  Xpos, unsigned int  Ypos)
{	
	TFT_Write_Reg(0x2a); 
	TFT_Write_Data16(Xpos);
	TFT_Write_Reg(0x2b); 
	TFT_Write_Data16(Ypos);

	TFT_Write_Reg(0x2c);	
} 
/*************************************************
函数名：TFT_Set_Region
功能：设置lcd显示区域，在此区域写点数据自动换行
入口参数：xy起点和终点
返回值：无
*************************************************/
void TFT_Set_Region(unsigned int xStar, unsigned int yStar,unsigned int xEnd,unsigned int yEnd)
{
	TFT_Write_Reg(0x2a); 
	TFT_Write_Data16(xStar);
	TFT_Write_Data16(xEnd);
	TFT_Write_Reg(0x2b); 
	TFT_Write_Data16(yStar);
	TFT_Write_Data16(yEnd);

	TFT_Write_Reg(0x2c);	
}
//--------------------------------------------------------------------------
void TFT_Draw_Point(int x0, int y0 ,unsigned int color) 
{
	TFT_Set_XY(x0,y0);//设置光标位置 
	TFT_Write_Data16(color); 

}
//--------------------------------------------------------------------------
void TFT_Draw_Rect(int x0, int y0, int x1, int y1,unsigned int color) 
{
	unsigned int i,j; 
	
	//-----------填充矩形区域---------------------------
	for(i=y0;i<=y1;i++)
	{													   	 	
		TFT_Set_XY(x0,i);//设置光标位置 
		
		for(j=x0;j<=x1;j++)
		{
			TFT_Write_Data16(color); 
		}
	}
}

//--------------------------------------------------------------------------
void TFT_Init(void)
{

	TFT_SET_CS;
	TFT_BLED_ON;
	
	TFT_RESET_RST;//复位
	Delay_Ms(50);
	TFT_SET_RST;
	Delay_Ms(50);
	TFT_RESET_CS;//使能

	TFT_Write_Reg(0xCB);  
	TFT_Write_Data8(0x39); 
	TFT_Write_Data8(0x2C); 
	TFT_Write_Data8(0x00); 
	TFT_Write_Data8(0x34); 
	TFT_Write_Data8(0x02); 

	TFT_Write_Reg(0xCF);  
	TFT_Write_Data8(0x00); 
	TFT_Write_Data8(0XC1); 
	TFT_Write_Data8(0X30); 

	TFT_Write_Reg(0xE8);  
	TFT_Write_Data8(0x85); 
	TFT_Write_Data8(0x00); 
	TFT_Write_Data8(0x78); 

	TFT_Write_Reg(0xEA);  
	TFT_Write_Data8(0x00); 
	TFT_Write_Data8(0x00); 

	TFT_Write_Reg(0xED);  
	TFT_Write_Data8(0x64); 
	TFT_Write_Data8(0x03); 
	TFT_Write_Data8(0X12); 
	TFT_Write_Data8(0X81); 

	TFT_Write_Reg(0xF7);  
	TFT_Write_Data8(0x20); 

	TFT_Write_Reg(0xC0);    //Power control 
	TFT_Write_Data8(0x23);   //VRH[5:0] 

	TFT_Write_Reg(0xC1);    //Power control 
	TFT_Write_Data8(0x10);   //SAP[2:0];BT[3:0] 

	TFT_Write_Reg(0xC5);    //VCM control 
	TFT_Write_Data8(0x3e); //对比度调节
	TFT_Write_Data8(0x28); 

	TFT_Write_Reg(0xC7);    //VCM control2 
	TFT_Write_Data8(0x86);  //--

	TFT_Write_Reg(0x36);    // Memory Access Control 

	TFT_Write_Data8(0x48); //48 68竖屏//28 E8 横屏


	TFT_Write_Reg(0x3A);    
	TFT_Write_Data8(0x55); 

	TFT_Write_Reg(0xB1);    
	TFT_Write_Data8(0x00);  
	TFT_Write_Data8(0x18); 

	TFT_Write_Reg(0xB6);    // Display Function Control 
	TFT_Write_Data8(0x08); 
	TFT_Write_Data8(0x82);
	TFT_Write_Data8(0x27);  

	TFT_Write_Reg(0xF2);    // 3Gamma Function Disable 
	TFT_Write_Data8(0x00); 

	TFT_Write_Reg(0x26);    //Gamma curve selected 
	TFT_Write_Data8(0x01); 

	TFT_Write_Reg(0xE0);    //Set Gamma 
	TFT_Write_Data8(0x0F); 
	TFT_Write_Data8(0x31); 
	TFT_Write_Data8(0x2B); 
	TFT_Write_Data8(0x0C); 
	TFT_Write_Data8(0x0E); 
	TFT_Write_Data8(0x08); 
	TFT_Write_Data8(0x4E); 
	TFT_Write_Data8(0xF1); 
	TFT_Write_Data8(0x37); 
	TFT_Write_Data8(0x07); 
	TFT_Write_Data8(0x10); 
	TFT_Write_Data8(0x03); 
	TFT_Write_Data8(0x0E); 
	TFT_Write_Data8(0x09); 
	TFT_Write_Data8(0x00); 

	TFT_Write_Reg(0XE1);    //Set Gamma 
	TFT_Write_Data8(0x00); 
	TFT_Write_Data8(0x0E); 
	TFT_Write_Data8(0x14); 
	TFT_Write_Data8(0x03); 
	TFT_Write_Data8(0x11); 
	TFT_Write_Data8(0x07); 
	TFT_Write_Data8(0x31); 
	TFT_Write_Data8(0xC1); 
	TFT_Write_Data8(0x48); 
	TFT_Write_Data8(0x08); 
	TFT_Write_Data8(0x0F); 
	TFT_Write_Data8(0x0C); 
	TFT_Write_Data8(0x31); 
	TFT_Write_Data8(0x36); 
	TFT_Write_Data8(0x0F); 

	TFT_Write_Reg(0x11);    //Exit Sleep 
	Delay_Ms(150); 
		
	TFT_Write_Reg(0x29);    //Display on 
	TFT_Write_Reg(0x2c); 
}


