#ifndef __ILI9341_H__
#define __ILI9341_H__	

/**************************************************************************
		macro declaration
**************************************************************************/
#define TFT_SET_RST     	GPIOA->BSRR = GPIO_Pin_4	
#define TFT_RESET_RST 	GPIOA->BRR = GPIO_Pin_4	
#define TFT_SET_DC      	GPIOA->BSRR = GPIO_Pin_3	
#define TFT_RESET_DC  	GPIOA->BRR = GPIO_Pin_3
#define TFT_SET_CS      	GPIOA->BSRR = GPIO_Pin_2	
#define TFT_RESET_CS 	GPIOA->BRR = GPIO_Pin_2

#define TFT_BLED_OFF      GPIOA->BSRR = GPIO_Pin_1	
#define TFT_BLED_ON 	GPIOA->BRR = GPIO_Pin_1

/**************************************************************************
		function declaration
**************************************************************************/

//--------------------------------------------------------------------------
void SPI_Write(unsigned char dat); 
//--------------------------------------------------------------------------
void TFT_Write_Data8(unsigned char dat); 
//--------------------------------------------------------------------------
 void TFT_Write_Data16(unsigned int dat);	  
//--------------------------------------------------------------------------
void TFT_Write_Reg(unsigned char dat)	 ;
//--------------------------------------------------------------------------
void TFT_Set_XY(unsigned int  Xpos, unsigned int  Ypos);
//--------------------------------------------------------------------------
void TFT_Set_Region(unsigned int xStar, unsigned int yStar,unsigned int xEnd,unsigned int yEnd);
//--------------------------------------------------------------------------
void TFT_Init(void);
//--------------------------------------------------------------------------
void TFT_Draw_Point(int x0, int y0 ,unsigned int color);
//--------------------------------------------------------------------------
void TFT_Draw_Rect(int x0, int y0, int x1, int y1,unsigned int color);


#endif  
	 
	 



